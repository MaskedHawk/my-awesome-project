export function fetchHelloWorld() {
    return fetch("https://athanor-back.herokuapp.com/controller/hello_world")
        .then(res => res.json())
        .catch(e => console.error(e));
}

