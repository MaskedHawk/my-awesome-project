import React, {useEffect, useState} from "react";
import {fetchHelloWorld} from "../services/fetch-hello-world";
import PropTypes from 'prop-types';

const HelloWorld = ({
    fetchHelloWorld
}) => {
    const [message, setMessage] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
            fetchHelloWorld().then(message => {
                setIsLoading(false);
                setMessage(message.response);
            })
        }, [fetchHelloWorld]
    );

    return (
        <>
            {isLoading ? "Loading" : message}
        </>
    );
}

HelloWorld.defaultProps = {
    fetchHelloWorld: fetchHelloWorld
}

HelloWorld.propTypes = {
    fetchHelloWorld: PropTypes.func.isRequired
}

export default HelloWorld;

