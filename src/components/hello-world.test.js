import React from "react";
import {act, render, screen} from "@testing-library/react";
import HelloWorld from "./hello-world";

describe("Hello Wolrd", () => {
  test('should display Loading when data not load', async () => {

    render(<HelloWorld/>);

    const linkElement = screen.getByText('Loading');

    expect(linkElement).toBeInTheDocument();

  });

  test('should display data when loaded', async () => {
    let fakeFetchHelloWorld = () => Promise.resolve({response: "Hello World"});

    await act(async () => {
      render(<HelloWorld fetchHelloWorld={fakeFetchHelloWorld}/>);
    })
    const linkElement = screen.getByText('Hello World');

    expect(linkElement).toBeInTheDocument();

  });

})
