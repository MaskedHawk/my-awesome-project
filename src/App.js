import HelloWorld from "./components/hello-world";
import React from "react";

const App = () => {
    return (
        <HelloWorld/>
    )
}

export default App;
